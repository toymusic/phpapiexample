<?php

include("../apishopping/systemConfig.php");
class main {
	private $dbReference;
	var $dbConnect;
	var $result;

	/**
	*
	*/
	function __construct(){

	}

	function __destruct() {

	}
	function getListTopProducts() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {

			$sql = "SELECT * FROM products ORDER BY saled DESC LIMIT 4;";
			$this->result = $this->dbConnect->query($sql);
			if ($this->result->num_rows > 0) {
			// output data of each row
			$resultSet = array();
			while($row = $this->result->fetch_assoc()) {
				$resultSet[] = $row;
			}
			$this->dbReference->sendResponse(200,'{"products":'.json_encode($resultSet).'}');
			} else {
			//echo "0 results";
			$this->dbReference->sendResponse(200,'{"products":null}');
			}

		}
	}
	function getListCategories() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {

			$sql = "SELECT * FROM categories;";
			$this->result = $this->dbConnect->query($sql);
			if ($this->result->num_rows > 0) {
			// output data of each row
			$resultSet = array();
			while($row = $this->result->fetch_assoc()) {
				$resultSet[] = $row;
			}
			$this->dbReference->sendResponse(200,'{"categories":'.json_encode($resultSet).'}');
			} else {
			//echo "0 results";
			$this->dbReference->sendResponse(200,'{"categories":null}');
			}

		}
	}
	function createUser() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			if (isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["name"])) {
				$sql = "INSERT INTO users (full_name, password, email) VALUES ('".$_POST["name"]."', '".$_POST["password"]."', '".$_POST["email"]."')";
				$query = mysqli_query($this->dbConnect, $sql);
				if ($query) {
					$this->dbReference->sendResponse(200,'{"status":'.json_encode($query).'}');
				} else {
					$this->dbReference->sendResponse(200,'{"status":'.json_encode($query).'}');
				}
			} else {
				$this->dbReference->sendResponse(404,"Can't Insert DB");
			}
		}
	}
	function changeInfoUser() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			if (!empty($_POST["id"])) {
				$sql = " UPDATE users SET full_name = '".$_POST["full_name"]."', address = '".$_POST["address"]."', phone = '".$_POST["phone"]."' WHERE id = ".$_POST["id"];
				$query = mysqli_query($this->dbConnect, $sql);
				if ($query) {
					$this->dbReference->sendResponse(200,'{"status":'.json_encode($query).'}');
				} else {
					$this->dbReference->sendResponse(200,'{"status":'.json_encode($query).'}');
				}
			} else {
				$this->dbReference->sendResponse(404,"Can't Update DB");
			}
		}
	}
	function getUser() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			if (isset($_POST["email"]) && isset($_POST["password"])) {
				$password = base64_encode($_POST["password"]);
				$sql = "SELECT * FROM users  WHERE email = '".$_POST["email"]."' AND password = '".base64_encode($_POST["password"])."'";
				$this->result = mysqli_query($this->dbConnect, $sql);
				if ($this->result->num_rows > 0) {
				// output data of each row
					$resultSet = array();
					while($row = $this->result->fetch_assoc()) {
						$resultSet[] = $row;
					}
					$this->dbReference->sendResponse(200,'{"user":'.json_encode($resultSet).'}');
				} else {
				//echo "0 results";
				$this->dbReference->sendResponse(200,'{"user":null}');
				}
			} else {
				$this->dbReference->sendResponse(404,"Can't Connect Database");
			}
		}
	}
	function sendOrder() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			$status = true;
			if (isset($_POST["user_id"])) {
				$sql = "INSERT INTO bill (user_id, total) VALUES ('".$_POST["user_id"]."', '".$_POST["total"]."')";
				$query = mysqli_query($this->dbConnect, $sql);
				if ($query) {
					$last_id = mysqli_insert_id($this->dbConnect);
					$list_order_products = json_decode($_POST["list_products_order"]);
					for ($i = 0; $i < sizeof($list_order_products); $i++) {
						$sql = "INSERT INTO bill_detail (bill_id, product_id, quantity, price) VALUES (".$last_id.",".$list_order_products[$i]->id.", ".$list_order_products[$i]->quantity.", ".$list_order_products[$i]->price.")";
						$query = mysqli_query($this->dbConnect, $sql);
						if (!$query) {
							$status = false;
						}
					}
				} else {
					$status = false;
				}
			} else {
				$status = false;
			}
			$this->dbReference->sendResponse(200,'{"status":'.json_encode($status).'}');
		}
	}
	function getListProductsByCategory() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			if (isset($_POST["category_id"])) {
				$sql = "SELECT * FROM products WHERE category_id = ".$_POST["category_id"];
				$this->result = $this->dbConnect->query($sql);
				if ($this->result->num_rows > 0) {
					// output data of each row
					$resultSet = array();
					while($row = $this->result->fetch_assoc()) {
						$resultSet[] = $row;
					}
					$this->dbReference->sendResponse(200,'{"products":'.json_encode($resultSet).'}');
				}
			} else {
				$this->dbReference->sendResponse(200,'{"products":null}');
			}

		}
	}
	function getOrderHistory() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			if (isset($_POST["user_id"])) {
				$sql = "SELECT * FROM bill WHERE user_id = ".$_POST["user_id"];
				$this->result = $this->dbConnect->query($sql);
				if ($this->result->num_rows > 0) {

					$resultSet = array();
					while($row = $this->result->fetch_assoc()) {
						$resultSet[] = $row;
					}
					$this->dbReference->sendResponse(200,'{"orders":'.json_encode($resultSet).'}');
				}
			} else {
				$this->dbReference->sendResponse(200,'{"orders":null}');
			}

		}
	}
	function searchProduct() {
		$this->dbReference = new systemConfig();
		$this->dbConnect = $this->dbReference->connectDB();
		if ($this->dbConnect == NULL) {
			$this->dbReference->sendResponse(503,'{"error_message":'.$this->dbReference->getStatusCodeMeeage(503).'}');
		} else {
			$sql = "SELECT * FROM products";
			if ($_POST["name"] != null) {
				$sql = "SELECT * FROM products WHERE name LIKE '%".$_POST["name"]."%';";
			}
			$this->result = $this->dbConnect->query($sql);
			if ($this->result->num_rows > 0) {
			// output data of each row
			$resultSet = array();
			while($row = $this->result->fetch_assoc()) {
				$resultSet[] = $row;
			}
			$this->dbReference->sendResponse(200,'{"products":'.json_encode($resultSet).'}');
			} else {
			//echo "0 results";
			$this->dbReference->sendResponse(200,'{"products":null}');
			}

		}
	}
}

?>